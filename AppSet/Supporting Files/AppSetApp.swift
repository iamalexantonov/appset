//
//  AppSetApp.swift
//  AppSet
//
//  Created by Alexey Antonov on 18/05/21.
//

import SwiftUI
import Firebase

@main
struct AppSetApp: App {
    init() {
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
