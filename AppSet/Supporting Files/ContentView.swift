//
//  ContentView.swift
//  AppSet
//
//  Created by Alexey Antonov on 18/05/21.
//

import SwiftUI

struct ContentView: View {
    @State private var showAdd = false
    
    var body: some View {
        NavigationView {
            HomeView()
                .navigationBarTitleDisplayMode(.inline)
                
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
