//
//  ReactionSheetView.swift
//  AppSet
//
//  Created by Mirella Cetronio on 20/05/21.
//

import SwiftUI

struct ReactionSheetView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State private var reactionList = 0
    
    var body: some View {
        VStack {

            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }, label: {
                RoundedRectangle(cornerRadius: 5)
                    .foregroundColor(.gray)
                    .frame(width: 70, height: 7)
                
            })
            .padding()
            
            Picker(selection: $reactionList, label: Text("Reactions")) {
                Text("All").tag(0)
                Text("👍🏻").tag(1)
                Text("👏🏻").tag(2)
                Text("👎🏻").tag(3)
                Text("🤌🏻").tag(4)
            }
            .pickerStyle(SegmentedPickerStyle())
            .padding(.bottom, 30)
            
            ScrollView {
                switch reactionList {
                case 1:
                    ForEach (0 ..< 5, id: \.self) { _ in
                        ZStack {
                            RoundedRectangle(cornerRadius: 15)
                                .fill(Color.lightGray)
                                .frame(height: 40)
                            HStack {
                                Text("T")
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .font(.largeTitle)
                                    .background(Color.random)
                                    .clipShape(Circle())
                                Spacer()
                                Text("Team Name")
                                    .font(.headline)
                                    .foregroundColor(.black)
                                Spacer()
                                Text("👍🏻")
                                    .padding(.all, 5.0)
                                    .background(Color.btnColor)
                                    .cornerRadius(10)
                                    .offset(y: 15)
                            }
                            .padding(.horizontal, 15)
                        }
                    }
                case 2:
                    ForEach (0 ..< 5, id: \.self) { _ in
                        ZStack {
                            RoundedRectangle(cornerRadius: 15)
                                .fill(Color.lightGray)
                                .frame(height: 40)
                            HStack {
                                Text("T")
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .font(.largeTitle)
                                    .background(Color.random)
                                    .clipShape(Circle())
                                Spacer()
                                Text("Team Name")
                                    .font(.headline)
                                    .foregroundColor(.black)
                                Spacer()
                                Text("👏🏻")
                                    .padding(.all, 5.0)
                                    .background(Color.btnColor)
                                    .cornerRadius(10)
                                    .offset(y: 15)
                            }
                            .padding(.horizontal, 15)
                        }
                    }
                case 3:
                    ForEach (0 ..< 5, id: \.self) { _ in
                        ZStack {
                            RoundedRectangle(cornerRadius: 15)
                                .fill(Color.lightGray)
                                .frame(height: 40)
                            HStack {
                                Text("T")
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .font(.largeTitle)
                                    .background(Color.random)
                                    .clipShape(Circle())
                                Spacer()
                                Text("Team Name")
                                    .font(.headline)
                                    .foregroundColor(.black)
                                Spacer()
                                Text("👎🏻")
                                    .padding(.all, 5.0)
                                    .background(Color.btnColor)
                                    .cornerRadius(10)
                                    .offset(y: 15)
                            }
                            .padding(.horizontal, 15)
                        }
                    }
                case 4:
                    ForEach (0 ..< 5, id: \.self) { _ in
                        ZStack {
                            RoundedRectangle(cornerRadius: 15)
                                .fill(Color.lightGray)
                                .frame(height: 40)
                            HStack {
                                Text("T")
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .font(.largeTitle)
                                    .background(Color.random)
                                    .clipShape(Circle())
                                Spacer()
                                Text("Team Name")
                                    .font(.headline)
                                    .foregroundColor(.black)
                                Spacer()
                                Text("🤌🏻")
                                    .padding(.all, 5.0)
                                    .background(Color.btnColor)
                                    .cornerRadius(10)
                                    .offset(y: 15)
                            }
                            .padding(.horizontal, 15)
                        }
                    }
                default:
                    ForEach (0 ..< 5, id: \.self) { _ in
                        ZStack {
                            RoundedRectangle(cornerRadius: 15)
                                .fill(Color.lightGray)
                                .frame(height: 40)
                            HStack {
                                Text("T")
                                    .frame(width: 50, height: 50, alignment: .center)
                                    .font(.largeTitle)
                                    .background(Color.random)
                                    .clipShape(Circle())
                                Spacer()
                                Text("Team Name")
                                    .font(.headline)
                                    .foregroundColor(.black)
                                Spacer()
                                Text("👍🏻")
                                    .padding(.all, 5.0)
                                    .background(Color.btnColor)
                                    .cornerRadius(10)
                                    .offset(y: 15)
                            }
                            .padding(.horizontal, 15)
                        }
                    }
                }
            }
            
//            Spacer()
        }
        .padding(.bottom, (UIApplication.shared.windows.last?.safeAreaInsets.bottom)! + 10)
        .padding(.horizontal, 30)
        .background(Color.white)
        .cornerRadius(15)
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct ReactionSheetView_Previews: PreviewProvider {
    static var previews: some View {
        ReactionSheetView()
    }
}
