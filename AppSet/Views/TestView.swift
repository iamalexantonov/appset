//
//  TestView.swift
//  AppSet
//
//  Created by Mirella Cetronio on 21/05/21.
//

import SwiftUI

struct TestView: View {
    @State private var showReactions = false
    @State var offset : CGFloat = UIScreen.main.bounds.height
    
    var body: some View {
        ZStack{
            
            Button(action: {
                self.offset = 0
            }) {
                Text("Action Sheet")
                    .foregroundColor(.blue)
            }
            
            
            VStack{
                
                Spacer()
                
                
                ReactionSheetView()
                    .frame(maxHeight: 600)
                    .offset(y: self.offset)
                    .gesture(DragGesture()
                                
                                .onChanged({ (value) in
                                    
                                    if value.translation.height > 0 {
                                        
                                        self.offset = value.location.y
                                        
                                    }
                                })
                                .onEnded({ (value) in
                                    
                                    if self.offset > 100 {
                                        
                                        self.offset = UIScreen.main.bounds.height
                                    } else {
                                        
                                        self.offset = 0
                                    }
                                })
                    )
                
            }.background((self.offset <= 100 ? Color(UIColor.label).opacity(0.3) : Color.clear).edgesIgnoringSafeArea(.all)
                            .onTapGesture {
                                
                                self.offset = 0
                                
                            })
            
            .edgesIgnoringSafeArea(.bottom)
            
        }.animation(.default)
        .navigationTitle("AppSet")
        
    }
}

struct TestView_Previews: PreviewProvider {
    static var previews: some View {
        TestView()
    }
}


