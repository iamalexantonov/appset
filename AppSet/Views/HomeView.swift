//
//  HomeView.swift
//  AppSet
//
//  Created by Mirella Cetronio on 20/05/21.
//

import SwiftUI

struct HomeView: View {
    @StateObject private var viewModel: HomeViewModel
    
    private var teamsItem: [GridItem] = Array(repeating: GridItem(.fixed(160), spacing: 20), count: 2)
    
    @State var pinCode: String = ""
    @State private var isCreating = false
    @State private var isJoining = false
    
    @State private var goesToFeedback = false
    
    init() {
        self._viewModel = StateObject<HomeViewModel>(wrappedValue: HomeViewModel(service: FirebaseWebService()))
        
        let appearance = UINavigationBarAppearance()
//        appearance.configureWithTransparentBackground()
        
//        appearance.largeTitleTextAttributes = [
//            NSAttributedString.Key.foregroundColor : UIColor.black,
//            .font : UIFont(name: "HelveticaNeue-Thin", size: 20)!]
        
        appearance.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.black]
        
        UINavigationBar.appearance().tintColor = UIColor.black
        
        // In the following two lines you make sure that you apply the style for good
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().standardAppearance = appearance
    }
    
    var body: some View {
            ScrollView {
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200)
                    .padding(.top, -30)
                HStack {
                    HStack {
                        TextField("Insert code...", text: $pinCode)
                            .foregroundColor(.gray)
                        Image(systemName: "delete.left")
                            .foregroundColor(.gray)
                            .opacity(pinCode == "" ? 0 : 1)
                            .onTapGesture { self.pinCode = "" }
                    }
                    .modifier(customTextField())
                    
                    Button(action: {
                        isJoining = true
                    }, label: {
                        Text("Join")
                    })
                    .buttonStyle(smallButton())
                    .disabled(pinCode.isEmpty)
                    .opacity(pinCode.isEmpty ? 0.5 : 1)
                    .fullScreenCover(isPresented: $isJoining) { JoinView(onJoin: { name in
                        UserDefaults.standard.setValue(name, forKey: "userName")
                        viewModel.roomToEnter = pinCode
                    }) }
                }
                .padding(30)
                .padding(.top, -70)
                
                Divider()
                
                Text("Your Teams")
                    .font(.title)
                    .padding(.top, 30)
                
                LazyVGrid(columns: teamsItem, spacing: 20) {
                    ForEach (viewModel.rooms, id: \.self) { room in
                        //NavigationLink(destination: TeamListView()) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 20)
                                    .fill(Color.random)
                                    .frame(height: 100)
                                NavigationLink(
                                    destination: FeedbackSessionView(viewModel:  FeedbackSessionViewModel(dbService: FirebaseWebService(), room: room)),
                                    tag: room,
                                    selection: $viewModel.roomToEnter,
                                    label: {
                                        VStack {
                                            Text(room)
                                                .font(.headline)
                                            Text("4 members")
                                                .font(.subheadline)
                                        }
                                        .foregroundColor(.white)
                                    })
                            }
                        //}
                    }
                }
            }
            .onAppear(perform: {
                viewModel.getRooms()
            })
            .navigationTitle("AppSet")
            .navigationBarItems(trailing:
                                    Button(action: {
                                        isCreating.toggle()
                                    }, label: {
                                        Image(systemName: "plus")
                                            .font(.system(size: 30))

                                    })
                                    .fullScreenCover(isPresented: $isCreating) { CreationView() { teamName in
                                        viewModel.createRoom(teamName: teamName)
                                        viewModel.roomToEnter = teamName
                                        //viewModel.getRooms()
                                        
                                    } }
            )
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
