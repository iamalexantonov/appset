//
//  TeamListView.swift
//  AppSet
//
//  Created by Mirella Cetronio on 20/05/21.
//

import SwiftUI

struct TeamListView: View {
    
    @State var pinCode: String = ""
    @State private var isCreating = false
    @State private var isJoining = false
    @State private var isSharing = false
    
    
    var body: some View {
        
        ForEach (0 ..< 5, id: \.self) { _ in
            ZStack {
                RoundedRectangle(cornerRadius: 15)
                    .fill(Color.lightGray)
                    .frame(height: 40)
                HStack {
                    Text("T")
                        .frame(width: 50, height: 50, alignment: .center)
                        .font(.largeTitle)
                        .background(Color.random)
                        .clipShape(Circle())
                    Spacer()
                    Text("Team Name")
                        .font(.headline)
                        .foregroundColor(.black)
                    Spacer()
                }
                .padding(.horizontal, 15)
            }
            .padding(.horizontal, 30)
            .padding(.vertical, 10)
        }
        .navigationTitle("Team Name")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing:
                                HStack {
                                    Button(action: shareBtn) {
                                        Image(systemName: "square.and.arrow.up")
                                            .font(.system(size: 30))

                                    }
                                    
//                                    Button(action: {
//                                        print("edit")
//                                    }, label: {
//                                        Image(systemName: "square.and.pencil")
//                                    })
                                })
        
    }
    
    func shareBtn() {
        isSharing.toggle()
        let codePin = "1234"
        let av = UIActivityViewController(activityItems: [codePin], applicationActivities: nil)
        
        UIApplication.shared.windows.first?.rootViewController?.present(av, animated: true, completion: nil)
    }
}

struct TeamListView_Previews: PreviewProvider {
    static var previews: some View {
        TeamListView()
    }
}
