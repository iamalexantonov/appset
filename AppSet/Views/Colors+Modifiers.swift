//
//  Colors+Modifiers.swift
//  AppSet
//
//  Created by Mirella Cetronio on 20/05/21.
//

import SwiftUI



extension Color {
    static let random = Color(red: .random(in: 0...255)/255, green: .random(in: 0...255)/255, blue: .random(in: 0...255)/255)
}

extension Color {
    static let lightGray = Color(red: 229/255, green: 229/255, blue: 229/255)
}

extension Color {
    static let btnColor = Color(red: 252/255, green: 97/255, blue: 9/255)
}

extension Color {
    static let ctaColor = Color(red: 252/255, green: 155/255, blue: 9/255)
}



struct smallButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding(10)
            .padding(.horizontal, 15)
            .background(Color.btnColor)
            .foregroundColor(.white)
            .cornerRadius(10)
    }
}

struct bigButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .frame(width: UIScreen.main.bounds.width - 60, height: 40, alignment: .center)
            .background(Color.ctaColor)
            .foregroundColor(.white)
            .cornerRadius(10)
    }
}

struct customTextField: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding(10)
            .foregroundColor(.gray)
            .background(Color.lightGray)
            .cornerRadius(10)
    }
}
