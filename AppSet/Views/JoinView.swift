//
//  JoinView.swift
//  AppSet
//
//  Created by Mirella Cetronio on 20/05/21.
//

import SwiftUI

struct JoinView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var memberName: String = ""
    
    let onJoin: (String) -> Void
    
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text("Join: Team Name")
                    .font(.headline)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "multiply")
                        .foregroundColor(.black)
                        .font(.system(size: 30))

                })
            }
            .padding()
            
            Spacer()
            
            VStack(alignment: .leading) {
                Text("Insert Your name")
                HStack {
                    TextField("Your name...", text: $memberName)
                        .foregroundColor(.gray)
                    Image(systemName: "delete.left")
                        .foregroundColor(.gray)
                        .opacity(memberName == "" ? 0 : 1)
                        .onTapGesture { self.memberName = "" }
                }
                .modifier(customTextField())
            }
            .padding(30)
            
            Spacer()
            
            Button(action: {
                if !memberName.isEmpty {
                    onJoin(memberName)
                    presentationMode.wrappedValue.dismiss()
                }
            }, label: {
                Text("Join")
                    .font(.headline)
            })
            .buttonStyle(bigButton())
            .opacity(memberName.isEmpty ? 0.5 : 1)
            .disabled(memberName.isEmpty)
            .padding(.bottom, 150)
        }
    }
}

struct JoinView_Previews: PreviewProvider {
    static var previews: some View {
        JoinView(onJoin: { print($0) })
    }
}
