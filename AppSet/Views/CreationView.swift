//
//  CreationView.swift
//  AppSet
//
//  Created by Mirella Cetronio on 20/05/21.
//

import SwiftUI

struct CreationView: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State var userName: String = ""
    @State var teamName: String = ""
    
    let onClose: (String)->Void
    
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text("Team creation")
                    .font(.headline)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "multiply")
                        .foregroundColor(.black)
                        .font(.system(size: 30))

                })
            }
            .padding()
            
            Spacer()
            
            VStack(alignment: .leading) {
                Text("Insert Team name")
                HStack {
                    TextField("Team name...", text: $teamName)
                        .foregroundColor(.gray)
                    Image(systemName: "delete.left")
                        .foregroundColor(.gray)
                        .opacity(teamName == "" ? 0 : 1)
                        .onTapGesture { self.teamName = "" }
                }
                .modifier(customTextField())
            }
            .padding(30)
            
            VStack(alignment: .leading) {
                Text("Insert Your name")
                HStack {
                    TextField("Your name...", text: $userName)
                        .foregroundColor(.gray)
                    Image(systemName: "delete.left")
                        .foregroundColor(.gray)
                        .opacity(userName == "" ? 0 : 1)
                        .onTapGesture { self.userName = "" }
                }
                .modifier(customTextField())
            }
            .padding(30)
            
            Spacer()
            
            Button(action: {
                onClose(teamName)
                UserDefaults.standard.setValue(userName, forKey: "userName")
                presentationMode.wrappedValue.dismiss()
            }, label: {
                Text("Join")
                    .font(.headline)
            })
            .buttonStyle(bigButton())
            .padding(.bottom, 150)
            .disabled(teamName.isEmpty || userName.isEmpty)
            .opacity(teamName.isEmpty || userName.isEmpty ? 0.5 : 1)
        }
    }
}

struct CreationView_Previews: PreviewProvider {
    static var previews: some View {
        CreationView(onClose: { print($0) })
    }
}
