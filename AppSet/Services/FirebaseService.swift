//
//  FirebaseService.swift
//  AppSet
//
//  Created by Alexey Antonov on 19/05/21.
//

import Foundation
import Firebase

protocol FeedbackWebService {
    var ref: DatabaseReference! { get set }
    
    func joinRoom(teamName: String, onRecieve: @escaping (Feedback)->Void)
    func getSession(sessionID: UUID)
    func send(feedback: String, for team: String)
    func rate(feedback: Feedback, with rating: Int)
    func createRoom(for team: String)
    func getRooms(completion: @escaping ([String])->Void)
}

final class FirebaseWebService: FeedbackWebService {
    var ref: DatabaseReference!
    
    init() {
        self.ref = Database.database().reference()
    }
    
    func joinRoom(teamName: String, onRecieve: @escaping (Feedback)->Void) {
        ref.child(teamName).child("session").observe(.childAdded) { snapshot in
            if snapshot.exists() {
                let id = snapshot.key as String
                if let data = snapshot.value as? [String: String] {
                    let text = data["feedbackText"] ?? ""
                    let userName = data["userName"] ?? ""
                    let feedback = Feedback(id: id, feedbackText: text, userName: userName)
                    DispatchQueue.main.async {
                        onRecieve(feedback)
                    }
                }
            }
        }
    }
    
    func getSession(sessionID: UUID) {
        
    }
    
    func send(feedback: String, for team: String) {
        struct FeedbackForm: Codable {
            let feedbackText: String
            let name: String
        }
        
        let feedbackToSend = [
            "feedbackText": feedback,
            "userName": UserDefaults.standard.string(forKey: "userName") ?? ""
        ]
        
        ref.child(team).child("session").childByAutoId().setValue(feedbackToSend)
    }
    
    func rate(feedback: Feedback, with rating: Int) {
        
    }
    
    func createRoom(for team: String) {
        ref.child(team).child("members").childByAutoId().setValue("Name")
    }
    
    func getRooms(completion: @escaping ([String])->Void) {
        ref.getData { error, snapshot in
            if let error = error {
                print(error.localizedDescription)
            } else if snapshot.exists() {
                if let data = snapshot.value as? [String: Any] {
                    DispatchQueue.main.async {
                        var rooms = [String]()
                        for key in data.keys {
                            rooms.append(key as String)
                        }
                        completion(rooms)
                    }
                } else {
                    print("Fail")
                }
            } else {
                completion([])
            }
        }
    }
    
}
