//
//  AIService.swift
//  AppSet
//
//  Created by Alexey Antonov on 19/05/21.
//

import Foundation
import NaturalLanguage
import CoreML

final class AIService {
    private let mlModel: MLModel
    private let offensiveFeedbackPredictor: NLModel
        
    init() {
        do {
            self.mlModel = try FeedbackClassifyer(configuration: MLModelConfiguration()).model
            self.offensiveFeedbackPredictor = try NLModel(mlModel: mlModel)
        } catch {
            fatalError("There is a problem with AI model")
        }
    }
    
    func howOffensive(is text: String) -> Double {
        offensiveFeedbackPredictor.predictedLabelHypotheses(for: text, maximumCount: 2).filter({ $0.key == "OFF" }).first?.value ?? 0
    }
}
