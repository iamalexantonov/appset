//
//  Feedback.swift
//  AppSet
//
//  Created by Alexey Antonov on 18/05/21.
//

import Foundation

enum FeedbackRating: Int, Codable {
    case bad = -1
    case neutral = 0
    case good = 1
}

struct Feedback: Identifiable, Codable {
    let id: String
    let feedbackText: String
    let userName: String
    //let feedbackRating: [FeedbackRating]
    
//    var totalPoints: Int {
//        feedbackRating.reduce(0, { $0 + $1.rawValue })
//    }
}
