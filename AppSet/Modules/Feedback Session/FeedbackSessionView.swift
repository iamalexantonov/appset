//
//  FeedbackSessionView.swift
//  AppSet
//
//  Created by Alexey Antonov on 18/05/21.
//

import SwiftUI

struct FeedbackSessionView: View {
    @State private var showAdd = false
    @StateObject private var viewModel: FeedbackSessionViewModel
    
    init(viewModel: FeedbackSessionViewModel) {
        self._viewModel = StateObject<FeedbackSessionViewModel>(wrappedValue: viewModel)
    }
    
    var body: some View {
    
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                if viewModel.feedbacks.count > 0 {
                    ForEach(viewModel.feedbacks) { feedback in
                        FeedbackCell(username: feedback.userName, text: feedback.feedbackText)
                            .padding(.vertical, 20)
                    }
                } else {
                    Text("Be the first who adds a feedback")
                    Spacer()
                }
            }
           
        }
        .onAppear(perform: {
            viewModel.subscribeToRoom()
        })
        .navigationTitle("Feedback Session")
        .navigationBarItems(trailing: Button(action: {
            showAdd = true
        }, label: {
            Image(systemName: "plus")
                .foregroundColor(.black)
                .font(.system(size: 30))
                
        }))
        .fullScreenCover(isPresented: $showAdd) { AddFeedbackView() { feedbackText in
            viewModel.send(feedback: feedbackText)
        } }
//        .sheet(isPresented: $showAdd, content: { AddFeedbackView() })
    }
}

struct FeedbackCell: View {
    let username: String
    let text: String
    let time = Int()
    
    //let rgbColor = Color(red: 1.0, green: 0.5, blue: 0.5)
    var body: some View {
        VStack {
            
            HStack {
                Text("From:")
                    .font(.footnote)
                    .fontWeight(.regular)
                    .foregroundColor(Color.gray)
                Text(username)
                    .foregroundColor(.black)
                    .bold()
                Spacer()
                Text("\(time) h")
                    .font(.caption)
                    .foregroundColor(Color.gray)
                    .multilineTextAlignment(.leading)
            }
            .padding(.horizontal, 10)
            .padding(.bottom, -5)
            
            HStack {
                Text(text)
                    .lineLimit(nil)
                    .padding()
                Spacer()
            }
            .background(Color.lightGray)
//            .background(Color(red: 0.898, green: 0.898, blue: 0.898))
            .cornerRadius(16)
               
            RateButton(ratingSelected: {
                print($0)
            })
            .padding(.horizontal, 10)
            .padding(.top, -20)
        }.padding(.horizontal)
    }
}

struct RateButton: View {
    @State private var showsChoice = false
    @State private var counts = 0
    @State private var shouldHide = false
    let ratingSelected: (Int) -> Void
   
    var body: some View {
    
        HStack {
            Text("\(counts) reactions")
                .multilineTextAlignment(.leading)
                .font(.caption)
                .foregroundColor(Color.gray)
                .padding(.top)
            
            Spacer()
     
            VStack {
                Button(action: {
                    withAnimation {
                        showsChoice = true
                        shouldHide.toggle()
                        
                    }
                    
                }) {
                    Text("👍🏻")
                        .padding(.all, 5.0)
                        .background(Color.btnColor)
//                        .background(Color(red: 0.989, green: 0.379, blue: 0.029))
                    
                }
                
                .opacity(showsChoice ? 0 : 1)
                .cornerRadius(10)
            }
            
            if shouldHide {
                ZStack {
                    
                    /*     Button(action: {
                     withAnimation {
                     showsChoice = true
                     }
                     
                     }) {
                     Text("👍🏻")
                     
                     }
                     .opacity(showsChoice ? 0 : 1)*/
                    
                    HStack(spacing: 8) {
                        
                        Button(action: {
                            withAnimation {
                                showsChoice = false
                                counts += 1
                                shouldHide = false
                            }
                            ratingSelected(1)
                        }, label: {
                            Text("👍🏻")
                        })
                        Button(action: {
                            withAnimation {
                                showsChoice = false
                                counts += 1
                                shouldHide = false
                                
                            }
                            ratingSelected(0)
                        }, label: {
                            Text("👏🏻")
                        })
                        Button(action: {
                            withAnimation {
                                showsChoice = false
                                counts += 1
                                shouldHide = false
                                
                            }
                            ratingSelected(-1)
                        }, label: {
                            Text("👎🏻")
                        })
                        Button(action: {
                            withAnimation {
                                showsChoice = false
                                counts += 1
                                shouldHide = false
                                
                            }
                            ratingSelected(-1)
                        }, label: {
                            Text("🤌🏻")
                            
                        })
                        
                    }.opacity(showsChoice ? 1 : 0)
                }.padding(.vertical, 6)
                .padding(.horizontal, 5)
                .frame(width: 130)
                .foregroundColor(.white)
                .background(Color.btnColor)
//                .background(Color(red: 0.989, green: 0.379, blue: 0.029))
                .cornerRadius(10)
                
                
            }
        }
        }
}

