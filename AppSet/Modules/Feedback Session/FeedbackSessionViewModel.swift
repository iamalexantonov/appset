//
//  FeedbackSessionViewModel.swift
//  AppSet
//
//  Created by Alexey Antonov on 21/05/21.
//

import Foundation

final class FeedbackSessionViewModel: ObservableObject {
    private let dbService: FeedbackWebService
    let room: String
    
    @Published private(set) var feedbacks = [Feedback]()
    
    init(dbService: FeedbackWebService, room: String) {
        self.dbService = dbService
        self.room = room
    }
    
    func subscribeToRoom() {
        dbService.joinRoom(teamName: room) { [weak self] feedback in
            self?.feedbacks.append(feedback)
        }
    }
    
    func send(feedback: String) {
        dbService.send(feedback: feedback, for: room)
    }
}
