//
//  HomeViewModel.swift
//  AppSet
//
//  Created by Alexey Antonov on 21/05/21.
//

import Foundation

final class HomeViewModel: ObservableObject {
    private let dbService: FeedbackWebService
    
    @Published private(set) var rooms = [String]()
    @Published var roomToEnter: String?
    
    init(service: FeedbackWebService) {
        self.dbService = service
    }
    
    func getRooms() {
        dbService.getRooms { [weak self] rooms in
            DispatchQueue.main.async {
                self?.rooms = rooms
            }
        }
    }
    
    func createRoom(teamName: String) {
        dbService.createRoom(for: teamName)
    }
}
