//
//  AddFeedbackViewModel.swift
//  AppSet
//
//  Created by Alexey Antonov on 18/05/21.
//

import Foundation

final class AddFeedbackViewModel: ObservableObject {
    @Published var feedbackText = ""
    @Published var review: String?
    
    private let aiService = AIService()
    
    var isSendButtonDisabled: Bool {
        feedbackText.isEmpty
    }
    
    func sendFeedback(sendText: @escaping (String)->Void) {
        switch feedbackText.count {
        case let count where count < 25:
            review = "Your feedback is too short!\nTry to be more specific"
        case let count where count > 1500:
            review = "Your feedback is too long!\nTry to be more specific"
        default:
            let aiResult = getAdvice(for: aiService.howOffensive(is: feedbackText))
            if aiResult == "Good! Sending..." {
                sendText(feedbackText)
            } else {
                review = aiResult
            }
        }
    }
    
    private func getAdvice(for percentageOffensive: Double) -> String {
        switch percentageOffensive {
        case let percentage where percentage < 0.5:
            return "Good! Sending..."
        case let percentage where 0.5...0.75 ~= percentage:
            return "Your feedback seems to be a bit off the good standards. Take a minute to think. And here is advice: ..."
        default:
            return "Your feedback seems to be kind of offensive. Try to review it and avoid ... And here is advice: ..."
        }
    }
}

extension String: Identifiable {
    public var id: String { self }
}
