//
//  AddFeedbackView.swift
//  AppSet
//
//  Created by Alexey Antonov on 19/05/21.
//

import SwiftUI

struct AddFeedbackView: View {
    @StateObject private var viewModel = AddFeedbackViewModel()
    
    @Environment(\.presentationMode) var presentationMode
    
    let onSubmitFeedback: (String)->Void
    
    init(onSubmit: @escaping (String)->Void) {
        self.onSubmitFeedback = onSubmit
        
        UITextView.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                Text(UserDefaults.standard.string(forKey: "userName") ?? "")
                    .font(.headline)
                Spacer()
                Button(action: {
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Image(systemName: "multiply")
                        .foregroundColor(.black)
                        .font(.system(size: 30))
                })
            }
            .padding(.horizontal, 30)
            Spacer()
            HStack {
                Text("Your feedback")
                    .font(.headline)
                    .padding(.top, 20)
                    .padding(.leading, 30)
                Spacer()
            }
            TextEditor(text: $viewModel.feedbackText)
                .padding()
                .background(Color.black.opacity(0.2))
                .cornerRadius(16)
                .frame(minHeight: 300)
                .padding(.horizontal, 30)
            Spacer()
            HStack {
                Button(action: {
                    viewModel.sendFeedback() { feedback in
                        onSubmitFeedback(feedback)
                        presentationMode.wrappedValue.dismiss()
                    }
                }, label: {
                    HStack {
                        Spacer()
                        Text("Submit")
                            .font(.title2)
                            .foregroundColor(.white)
                        Spacer()
                    }
                })
                .buttonStyle(bigButton())
//                .background(Color(red: 0.933, green: 0.578, blue: 0.034))
                .opacity(viewModel.isSendButtonDisabled ? 0.6 : 1)
                .disabled(viewModel.isSendButtonDisabled)
                .padding(.vertical, 20)
            }
        }
        .alert(item: $viewModel.review) { review in
            Alert(title: Text(review), primaryButton: .cancel(Text("Review")), secondaryButton: .destructive(Text("Send anyway"), action: {
                onSubmitFeedback(viewModel.feedbackText)
                presentationMode.wrappedValue.dismiss()
            }))
        }
    }
}

struct AddFeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        AddFeedbackView(onSubmit: { print($0) })
    }
}
